(function($) {
    $(document).ready(function() {

        $('.section4').parallax({
            imageSrc: 'images/section4-bg.jpg',
            speed: 0.8,
            naturalWidth: 1160,
            naturalHeight: 542
        });

//============================================================================================================
//============================================================================================================

        $('.animate-box').viewportChecker({
            classToAdd: 'up',
            offset: 1
        });
        $('.animate-box2').viewportChecker({
            classToAdd: 'right',
            offset: 1
        });

//============================================================================================================
//============================================================================================================

        $(window).on('scroll',function() {
            var scrolltop = $(this).scrollTop();

            if(scrolltop >= 117) {
                $('.main-nav').addClass('fixed-menu');
            }

            else if(scrolltop <= 117) {
                $('.main-nav').removeClass('fixed-menu');
            }
        });

        var lastId,
            topMenu = $("#top-nav"),
            topMenuHeight = topMenu.outerHeight()+15,
            menuItems = topMenu.find("a"),
            scrollItems = menuItems.map(function(){
                var item = $($(this).attr("href"));
                if (item.length) { return item; }
            });

        menuItems.click(function(e){
            var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
            $('html, body').stop().animate({
                scrollTop: offsetTop
            }, 600);
            e.preventDefault();
        });

        $(window).scroll(function(){
            var fromTop = $(this).scrollTop()+topMenuHeight;
            var cur = scrollItems.map(function(){
                if ($(this).offset().top < fromTop)
                    return this;
            });
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
                lastId = id;
                menuItems
                    .parent().removeClass("active")
                    .end().filter("[href=#"+id+"]").parent().addClass("active");
            }
        });

//============================================================================================================
//============================================================================================================

    })
})(jQuery);